$(function() {

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultView: 'agendaWeek',
        nowIndicator: true,
        allDaySlot: false,
        axisFormat: 'HH:mm',
        slotDuration: '00:15:00',
        snapDuration: '00:05:00',
        aspectRatio: 1.10,
        hiddenDays: [0],
        lang: 'fr',
        editable: true,
        eventLimit: true,
        height: 870,
        firstDay: 1
    })

});