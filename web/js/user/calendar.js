$(document).ready($(function() {
    $('#nav-link-calendar a').addClass('active');

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultView: 'agendaWeek',
        nowIndicator: true,
        allDaySlot: false,
        timeFormat: 'H:mm',
        slotDuration: '00:15:00',
        snapDuration: '00:05:00',
        aspectRatio: 1.10,
        lang: 'fr',
        eventLimit: true,
        height: 870,
        firstDay: 1
    })
    fillCalendar();

}));

function fillCalendar(){
    $(".booking").each(function(){
        $('#calendar').fullCalendar('renderEvent',
            {
                title: "Salle : "+$(this).find('.booking-title').text(),
                start: datetimeToCalendar($(this).find('.booking-begin').text()),
                end: datetimeToCalendar($(this).find('.booking-end').text()),
                id: $(this).find('.booking-id').text(),
                color: $(this).find('.booking-color').text(),
                borderColor: '#000',
            },
            true
        )
    });

}

function datetimeToCalendar(datetime){
    final_datetime = datetime.replace(' ','T')+':00';
    return final_datetime;
}