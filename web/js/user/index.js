$('#nav-link-home a').addClass('active');
$(function () {
    $('.input-group.date').datetimepicker({
        icons: {
            time: "fas fa-clock",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        format: 'DD/MM/YYYY, HH:mm',
        locale: 'fr'
    });
});

function sendBooking(){
    var dataSend = {};
    dataSend.start = $("#booking-start").val();
    dataSend.end = $("#booking-end").val();
    dataSend.seat = $("#booking-number").val();
    dataSend.isComputerised = $("#booking-isComputerised").prop('checked');
    console.log(dataSend);
    $.ajax({
        type: "POST",
        url: $('#fast_booking_url').text(),
        data: dataSend,
        success: function (data, dataType) {
            if(data['status']=='success'){
                Swal.fire(
                    'Succès.',
                    'Salle '+data['room']['name']+' réservée.',
                    'success'
                );
            }
            else if(data['status']=='no room'){
                Swal.fire({
                    type: 'error',
                    title: 'Réservation impossible',
                    text: "Aucunes salle n'est disponible selon vos critères.",
                })
            }

        }
    });
}