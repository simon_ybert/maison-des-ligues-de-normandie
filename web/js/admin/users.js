$(document).ready(function () {
    $('#dtDynamicVerticalScrollUsers').DataTable({
        "scrollY": "75vh",
        "scrollCollapse": true,
        "paging": false,
        "searching": false,
        "info": false,
    });
    $('#nav-link-users a').addClass('active');
});

$("#form-new-user").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.

    if ($('#users_plainPassword_first').val()!=$('#users_plainPassword_second').val()){
        Swal.fire({
            type: 'error',
            title: "Création de l'utilisateur impossible",
            text: 'Les mots de passe saisies sont différents',
        })
    }
    else {
        var form = $(this);
        var url = form.attr('action');
        $('#league_send_button').addClass('running');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {

                if(data['status']=='success'){
                    Swal.fire(
                        'Succès.',
                        'Utilisateur ajouté.',
                        'success'
                    );
                    console.log(data['id'])
                    html = '<tr id="user_'+data['id']+'">' +
                        '<td class="user-name">'+data['username']+'</td>' +
                        '<td class="user-email">'+data['email']+'</td>' +
                        '<td class="user-league">'+data['league']+'</td>' +
                        '<td class="user_id d-none">'+data['id']+'</td>' +
                        '</tr>';
                        $('#users_list').append(html);
                }
                else {
                    Swal.fire({
                        type: 'error',
                        title: "Création de l'utilisateur impossible",
                        text: "L'utilisateur n'a pas pu être créé",
                    })
                }
            }
        });
    }
});