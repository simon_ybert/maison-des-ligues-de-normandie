$(document).ready(function () {
    $('#dtDynamicVerticalScrollRoom').DataTable({
        "scrollY": "75vh",
        "scrollCollapse": true,
        "paging": false,
        "searching": false,
        "info": false,
    });
});

$("#form-new-room").submit(function(e){
    e.preventDefault();
});

function openRoomModal(room, action) {
    if(action){
        room_id = room.children('.room-id').text();
        room_seat = room.children('.room-seat').text();
        room_name = room.children('.room-name').text();
        room_isComputerised = room.children('.room-isComputerised').text();
        room_isActive = room.children('.room-isActive').text();
        $('#roomModalName').val(room_name);
        $('#roomModalSeat').val(room_seat);
        $('#roomModalId').text(room_id);
        $('#roomModalTitle h4').text('Modification de la salle');
        if(room_isComputerised=='Oui'){
            $('#roomModalIsComputerised').prop('checked', true);
        }
        else {
            $('#roomModalIsComputerised').prop('checked', false);
        }
        if(room_isActive=='Oui'){
            $('#roomModalIsActive').prop('checked', true);
        }
        else {
            $('#roomModalIsActive').prop('checked', false);
        }
    }
    else {
        $('#roomModalName').val('');
        $('#roomModalSeat').val(0);
        $('#roomModalIsComputerised').prop('checked', false);
        $('#roomModalIsActive').prop('checked', true);
        $('#roomModalId').text('0');
        $('#roomModalTitle h4').text("Création d'une salle");
    }
    $('#roomModal').modal('show');
}

function saveRoomModal(method) {
    room_id = $('#roomModalId').text();
    if (room_id=="0" && method=='PUT_POST')
    {
        method='POST'
    }
    else if(method=='PUT_POST')
    {
        method='PUT'
    }
    room_name = $('#roomModalName').val();
    room_seat = $('#roomModalSeat').val();
    room_isComputerised = $('#roomModalIsComputerised').prop('checked');
    room_isActive = $('#roomModalIsActive').prop('checked');

    var dataSend = {};
    dataSend.roomId = room_id;
    dataSend.roomName = room_name;
    dataSend.roomSeat = room_seat;
    dataSend.roomIsComputerised = room_isComputerised;
    dataSend.roomIsActive = room_isActive;
    console.log(dataSend);
    $.ajax({
        type: method,
        url: $('#url_ajax_edit_room').text(),
        data: dataSend,
        success: function (data, dataType) {
            console.log(data);
        }
    });

    room = $('#room_'+room_id);
    room.children('.room-name').text(room_name);
    room.children('.room-seat').text(room_seat);
    if(room_isComputerised){
        room.children('.room-isComputerised').text('Oui');
    }
    else {
        room.children('.room-isComputerised').text('Non');
    }
    if(room_isActive){
        room.children('.room-isActive').text('Oui');
        room.removeClass('deactivate-room');
    }
    else {
        room.children('.room-isActive').text('Non');
        room.addClass('deactivate-room');
    }

    $('#roomModal').modal('hide');
}

function sendNewRoom() {
    var dataSend = {};
    dataSend.action = 'new';
    dataSend.roomName = $('#room_name').val();
    dataSend.roomSeat = $('#room_seat').val();
    dataSend.roomIsComputerised = $('#room_isComputerised').prop('checked');
    dataSend.roomIsActive = 1;
    if(dataSend.roomName != "" || dataSend.roomSeat != ""){
        $('#room_send_button').addClass('running');
        $.ajax({
            type: "POST",
            url: $('#url_ajax_edit_room').text(),
            data: dataSend,
            success: function (data, dataType) {
                Swal.fire(
                    'Succès.',
                    'Nouvelle salle ajoutée.',
                    'success'
                );
                if (dataSend.roomIsComputerised){
                    computer = 'Oui'
                }
                else {
                    computer = 'Non'
                }
                html='<tr id="room_'+data+'" onclick="openModal($(this))">' +
                    '<td class="room-name">'+dataSend.roomName+'</td>' +
                    '<td class="room-seat">'+dataSend.roomSeat+'</td>' +
                    '<td class="room-isComputerised">'+computer+'</td>' +
                    '<td class="room-isActive d-none">Oui</td>' +
                    '<td class="room-id d-none">'+data+'</td>' +
                    '</tr>'
                $('#room_list').append(html);
                $('#room_send_button').removeClass('running');
            }
        });
    }
}

function deleteRoom(){
    dataSend = {};
    dataSend.action = 'delete';
    dataSend.roomId = $('#editRoomModalId').text();
    $.ajax({
        type: "POST",
        url: $('#url_ajax_edit_room').text(),
        data: dataSend,
        success: function (data, dataType) {
            console.log(data);
            Swal.fire(
                'Succès.',
                'Salle supprimée.',
                'success'
            );
            $('#editRoomModal').modal('hide');
            $('#room_'+dataSend.roomId).remove();
        }
    });
}