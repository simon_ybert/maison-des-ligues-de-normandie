$(document).ready(function () {
    $('#dtDynamicVerticalScrollLeague').DataTable({
        "scrollY": "75vh",
        "scrollCollapse": true,
        "paging": false,
        "searching": false,
        "info": false,
    });
    $('#nav-link-league a').addClass('active');
});

function openModal(league) {
    league_id = league.children('.league-id').text();
    league_name = league.children('.league-name').text();
    league_sport = league.children('.league-sport').text();
    league_phone = league.children('.league-phone').text();
    league_color = league.children('.league-color').children('.league-color-input').val();
    $('#editLeagueModalName').val(league_name);
    $('#editLeagueModalSport').val(league_sport);
    $('#editLeagueModalPhone').val(league_phone);
    $('#editLeagueModalColor').val(league_color);
    $('#editLeagueModalId').text(league_id);
    $('#editLeagueModal').modal('show');
}

function deleteLeague()
{
    console.log('test')
}

function saveModal() {
    var dataSend = {};
    dataSend.action = 'edit';
    dataSend.leagueId = $('#editLeagueModalId').text();
    dataSend.leagueName = $('#editLeagueModalName').val();
    dataSend.leagueSport = $('#editLeagueModalSport').val();
    dataSend.leaguePhone = $('#editLeagueModalPhone').val();
    dataSend.leagueColor = $('#editLeagueModalColor').val();

    $.ajax({
        type: "POST",
        url: $('#url_ajax_edit_league').text(),
        data: dataSend,
        success: function (data, dataType) {
            league = $('#league_'+dataSend.leagueId);
            league.children('.league-name').text(dataSend.leagueName);
            league.children('.league-sport').text(dataSend.leagueSport);
            league.children('.league-phone').text(dataSend.leaguePhone);
            league.children('.league-color').children('.league-color-input').val(dataSend.leagueColor);
            $('#editLeagueModal').modal('hide');
        }
    });
}

function deleteLeague(){
    dataSend = {};
    dataSend.action = 'delete';
    dataSend.leagueId = $('#editLeagueModalId').text();
    $.ajax({
        type: "POST",
        url: $('#url_ajax_edit_league').text(),
        data: dataSend,
        success: function (data, dataType) {
            Swal.fire(
                'Succès.',
                'Ligue supprimée.',
                'success'
            );
            $('#editLeagueModal').modal('hide');
            $('#league_'+dataSend.leagueId).remove();
        }
    });
}

function sendNewLeague(){

    var dataSend = {};
    dataSend.action = 'new';
    dataSend.leagueName = $('#league_name').val();
    dataSend.leagueSport = $('#league_sport').val();
    dataSend.leaguePhone = $('#league_phone').val();
    dataSend.leagueColor = $('#league_color').val();
    var canSend = true
    if(canSend){
        $('#league_send_button').addClass('running');
        $.ajax({
            type: "POST",
            url: $('#url_ajax_edit_league').text(),
            data: dataSend,
            success: function (data, dataType) {
                Swal.fire(
                    'Succès.',
                    'Nouvelle ligue ajoutée.',
                    'success'
                );
                html='<tr id="league_'+data+'" onclick="openModal($(this))">' +
                    '<td class="league-name">'+dataSend.leagueName+'</td>' +
                    '<td class="league-sport">'+dataSend.leagueSport+'</td>' +
                    '<td class="league-phone">'+dataSend.leaguePhone+'</td>' +
                    '<td class="league-color"><input class="league-color-input" type="color" value="'+dataSend.leagueColor+'" disabled></td>' +
                    '<td class="league-id d-none">'+data+'</td>' +
                    '</tr>';
                $('#league_list').append(html);
                $('#league_send_button').removeClass('running');
                $('#league_name').val('');
                $('#league_phone').val('');
                $('#league_sport').val('');
                $('#league_color').val('');
            }
        });
    }
}