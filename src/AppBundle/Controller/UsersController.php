<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Booking;
use AppBundle\Entity\Room;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class UsersController extends Controller
{
    public function indexAction()
    {
        $data = array(
            'bookings'=>$this->getRecentBooking()
        );
        return $this->render('@App/user/index.html.twig', $data);
    }

    public function fastBookingAjaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $start = date_create_from_format("d/m/Y H:i", $data['start']);
        $end = date_create_from_format("d/m/Y H:i", $data['end']);
        $rooms = $this->getDoctrine()->getRepository(Room::class)->findAllRoomToBook($data['seat'], $data['isComputerised']);
        foreach ($rooms as $room){
            $result = $this->getDoctrine()
                ->getRepository(Booking::class)
                ->findAllBookingOnTimeSlot($start->format('Y-m-d H:i'), $end->format('Y-m-d H:i'), $room['id']);
            if(count($result)==0){
                $booking = new Booking();
                $booking->setBegin($start);
                $booking->setEndb($end);
                $booking->setRoom($this->getDoctrine()->getRepository(Room::class)->find($room['id']));
                $booking->setTimestamp(new \DateTime());
                $booking->setLeague($this->getUser()->getLeague());
                $em->persist($booking);
                $em->flush();
                return new JsonResponse(array('status'=>'success','room'=>$room));
            }
        }
        return new JsonResponse(array('status'=>'no room'));
    }

    public function calendarAction()
    {
        $data = array(
            'bookings' => $this->getCalendarBooking()
        );
        return $this->render('@App/user/calendar.html.twig', $data);
    }

    public function bookingAction()
    {
        return $this->render('@App/user/index.html.twig');
    }

    public function listAction()
    {
        return $this->render('@App/user/index.html.twig');
    }

    private function getRecentBooking()
    {
        $league = $this->getUser()->getLeague();
        $booking = $this->getDoctrine()
            ->getRepository(Booking::class)
            ->findAllRecentBooking($league->getId());
        return $booking;
    }

    private function getCalendarBooking()
    {
        $data = array();
        $league = $this->getUser()->getLeague();
        $bookings = $this->getDoctrine()->getRepository(Booking::class)->findBy(array('league'=>$league->getId()));
        foreach ($bookings as $book){
            array_push($data, array(
                'id'=>$book->getId(),
                'begin'=>$book->getBegin(),
                'end'=>$book->getEndb(),
                'color'=>$league->getColor(),
                'title'=>$book->getRoom()->getName()
            ));
        }
        return $data;

    }
}