<?php

namespace AppBundle\Controller;

use AppBundle\Entity\League;
use AppBundle\Entity\Users;
use AppBundle\Entity\Room;
use AppBundle\Form\RoomType;
use AppBundle\Form\UsersType;
use Doctrine\Common\Util\Debug;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminController extends Controller
{
    public function indexAction()
    {
        $data = array(
            'rooms'=>$this->getDoctrine()->getRepository(Room::class)->findAll(),
            'leagues'=>$this->getDoctrine()->getRepository(League::class)->findAll(),
            'users'=>$this->getDoctrine()->getRepository(Users::class)->findBy(array('role'=>'ROLE_USER'))
            );
        return $this->render('@App/admin/index.html.twig', $data);
    }

    public function calendarAction()
    {
        return new Response('1');
    }

    public function leagueAction()
    {
        $data = array(
            'leagues'=>$this->getDoctrine()->getRepository(League::class)->findAll(),
        );
        return $this->render('@App/admin/league.html.twig', $data);
    }

    public function leagueEditAction(Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
//        if($data['action']=='delete'){
        if($request->getMethod()=='DELETE'){
            $league = $this->getDoctrine()->getRepository(League::class)->find($data['leagueId']);
            $em->remove($league);
            $em->flush();
            return new Response('deleted');
        }
        else if($request->getMethod()=='PUT'){
            $league = $this->getDoctrine()->getRepository(League::class)->find($data['leagueId']);
        }
        else if($request->getMethod()=='POST'){
            $league= new League();
        }
        $league->setName($data['leagueName']);
        $league->setSport($data['leagueSport']);
        $league->setPhone($data['leaguePhone']);
        $league->setColor($data['leagueColor']);
        $em->persist($league);
        $em->flush();
        return new Response($league->getId());
    }

    public function roomAction(Request $request)
    {
        $rooms = $this->getDoctrine()->getRepository(Room::class)->findAll();
        $form = $this->createForm(RoomType::class);
        $data = array(
            'rooms'=>$rooms,
            'form'=>$form->createView()
        );

        return $this->render('@App/admin/room.html.twig',$data);
    }

    public function roomEditAction(Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        if($request->getMethod()=='DELETE'){
            $room = $this->getDoctrine()->getRepository(Room::class)->find($data['roomId']);
            $em->remove($room);
            $em->flush();
            return new Response('deleted');
        }
        else if($request->getMethod()=='PUT'){
            $room = $this->getDoctrine()->getRepository(Room::class)->find($data['roomId']);
        }
        else if($request->getMethod()=='POST'){
            $room = new Room();
        }
        $room->setName($data['roomName']);
        $room->setSeat($data['roomSeat']);
        $room->setIsComputerised(($data['roomIsComputerised'] == "true") ? true : false);
        $room->setIsActive(($data['roomIsActive'] == "true") ? true : false);
        $em->persist($room);
        $em->flush();
        return new JsonResponse(array('status'=>'success'));
    }

    public function usersAction(Request $request) {
        $user = new Users();
        $users = $this->getDoctrine()->getRepository(Users::class)->findBy(array('role'=>'ROLE_USER'));
        $form = $this->createForm(UsersType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setRole('ROLE_USER');
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return new JsonResponse(array(
                'status'=>'success',
                'id'=>$user->getId(),
                'username'=>$user->getUsername(),
                'email'=>$user->getEmail(),
                'league'=>$user->getLeague()->getName()
            ));
        }

        $data = array(
            'users'=>$users,
            'form'=>$form->createView()
        );

        return $this->render('@App/admin/users.html.twig',$data);
    }

}
