<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RedirectController extends Controller
{
    public function redirectRoleAction()
    {
        $user = $this->getUser();
        if ($user == null){
            return $this->redirect($this->generateUrl('m2n_login'));
        }
        else if($user->getRole()=='ROLE_ADMIN'){
            return $this->redirect($this->generateUrl('m2n_admin_homepage'));
        }
        else {
            return $this->redirect($this->generateUrl('m2n_homepage'));
        }
    }
}
