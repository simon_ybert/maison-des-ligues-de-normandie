<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BookingRepository extends EntityRepository
{
    public function findAllRecentBooking($league_id)
    {
        $qb = $this->createQueryBuilder('b')
            ->where('b.begin >= :today')
            ->andWhere('b.league = :league_id')
            ->setParameter('today', new \DateTime())
            ->setParameter('league_id', $league_id)
            ->orderBy('b.begin', 'ASC')
            ->setMaxResults(10)
            ->getQuery();
        return $qb->execute();
    }

    public function findAllBookingOnTimeSlot($start, $end, $room_id)
    {
        $query = $this->createQueryBuilder('p')
            ->Where('p.begin between :begin AND :end
                OR p.endb between :begin AND :end
                OR :begin between p.begin AND p.endb
                OR :end between p.begin AND p.endb')
            ->andWhere('p.room = :room')
            ->setParameters(array(
                'begin' => $start,
                'end'=>$end,
                'room'=>$room_id))
            ->getQuery();
        return $query->execute();
    }
}