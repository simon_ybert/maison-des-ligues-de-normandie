<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Room
 *
 * @ORM\Table(name="room")
 * @ORM\Entity
 */

class Room
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $seat;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isComputerised;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="room")
     */
    private $booking;

    public function __construct()
    {
        $this->booking = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getSeat()
    {
        return $this->seat;
    }

    public function setSeat($seat)
    {
        $this->seat = $seat;
        return $this;
    }

    public function getIsComputerised()
    {
        return $this->isComputerised;
    }

    public function setIsComputerised($isComputerised)
    {
        $this->isComputerised = $isComputerised;
        return $this;
    }

    public function getIsActive()
    {
        return $this->isActive;
    }

    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }
}